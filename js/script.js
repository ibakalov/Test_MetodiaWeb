if ($(window).width() >= 750) {
function Shrink() {
    window.addEventListener('scroll', function (e) {
        var s1Height = $('.section1').height()/2,
            distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = s1Height,
        FixedGrDiv = $(".section1");
        if (distanceY > shrinkOn) {
            FixedGrDiv.addClass('smaller');
        } else {
            if ( FixedGrDiv.hasClass( 'smaller' ) ) {
                FixedGrDiv.removeClass('smaller');
            }
        }
    });
}
Shrink();
}
var menuTogle = $('#toggle'),
    toggleUl = $('.menu');
$(menuTogle).on('click', function () {
    if (toggleUl.css('display') == 'none') {
        toggleUl.css('display', 'block');
    }
    else {
        toggleUl.css('display', 'none');
    }
});
//if ($(window).width() <= 750) {
//    $('.toggleUl a').on('click', function () {
//        console.log(this);
//        toggleUl.css('display', 'none');
//    });
//}